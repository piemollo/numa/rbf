%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [M] = rbf_distance_matrix(imu)
%
% This function computes the distance matrix between interpolation points.
%
%  input: - imu: set of interpolation points
%
% output: - M: distance matrix
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [M] = rbf_distance_matrix(imu)
  % - Initiate
  [N,d] = size(imu);
  M = zeros(N,N);
  
  % - Fill
  for i = 1:(N-1)
    for j = (i+1):N
      M(i,j) = norm(imu(i,:) - imu(j,:),2);
      M(j,i) = norm(imu(i,:) - imu(j,:),2);
    end
  end
  
end