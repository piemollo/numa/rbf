% ====== ====== Typical interpolation illustration

% --- Set Radial-Basis Function
sig = 0.1;
phi = rbf_kernel('M4',sig);

% --- Set interval
mu_max =  1;
mu_min =  -1;
mu_n   =  9;
mu = [linspace(mu_min, mu_max, mu_n)]';
xx = [linspace(mu_min, mu_max, mu_n*200)]';

% --- Target function
f = @(x) exp(x .* cos(3*pi.*x)) ;
%f = @(x) (sin(2*pi.*x)).^4 ./(x+1) ;
%f = @(x) abs(x) ;

% --- Build interp. coefficients
[gamma, omega] = rbf_coef(phi, mu, f(mu), 'aff') ;

% --- Polyfit
p = polyfit(mu, f(mu), length(mu)-1);
ep= polyval(p, xx);

% --- Display
figure(1)
clf
hold on

% - Evaluation
yex = f(xx);
yin = rbf_val(phi, gamma, omega, mu, xx);

% - Plots
plot(xx, yex, '-r', 'linewidth', 2)
plot(xx, yin, '--b', 'linewidth', 2)
plot(xx, ep,  '--g', 'linewidth', 2)
plot(mu, f(mu), '*k', 'markersize', 5, 'linewidth', 2)

% - Legend
l=legend('f exact', 'I_f interp', 'Polyfit', 'I.P', 'Location', 'north');
set(l, 'fontsize', 15);

% - Overlay
xlabel('\mu');
ylabel('f(\mu)');
title('Interpolation example')
set(gca, 'fontsize', 15);