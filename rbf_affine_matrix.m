%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [A] = rbf_affine_matrix(phi, imu)
%
% This function computes the affine RBF matrix.
%
%  input: - phi: radial basis kernel
%         - imu: set of interpolation points
%
% output: - A: affine RBF matrix
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [A] = rbf_affine_matrix(phi, imu)
  % - Initiate
  [N,d] = size(imu) ;
  One = ones(1,N) ;
  
  % - Assembling
  M = rbf_linear_matrix(phi, imu);
  A = [M, imu One' ; imu' zeros(d,d) zeros(d,1) ; One zeros(1,d) 0];
  
end